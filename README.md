<h1>Introdução</h1>
<i>Este código, ele captura informações pessoais do R.A (Registro Acadêmico)</i>
<br>
<h3>As informaçes que o sistema retornarar são:</h3> 
  <ol>
    <li>Nome do Pai   </li>
    <li>Nome da Mãe   </li>
    <li>Estado Civil   </li>
    <li>CPF ( Se tiver registrado )   </li>
    <li>RG ( Se tiver registrado )   </li>
    <li>Cidade de Nasc. - UF   </li>
    <li>Endereço Residencial   </li>
  </ol>
<br>
<h3>Comando de uso </h3>
Para utilizar sao necessario informar ao nosso sistema 4 informações...<br>
<i>Codigo R.A (Exemplo: 000109165828 )</i><br>
<i>Digito do R.A (Exemplo: 1 )</i><br>
<i>UF (Estado que o R.A se encontra)</i><br>
<i>Data de Nascimento <b>%d/%m/%Y</b> (Exemplo 02/11/2017) </i>
<br>
<blockquote> Comando: <i>python pesquisar.py [R.A] [Digito do R.A] [UF] [Data Nascimento]</i>
<br>
Exemplo de Comando:  python pesquisar.py 1234567890 2 SP 01/01/1999</blockquote> 
